import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('chrony_group')


def test_chrony_package(host):
    assert host.package("chrony").is_installed


def test_chrony_conffile(host):
    if 'debian' in host.ansible.get_variables()["inventory_hostname"]:
        f = host.file("/etc/chrony/chrony.conf")
    else:
        f = host.file("/etc/chrony.conf")
    assert f.exists
    assert f.is_file


def test_chrony_service(host):
    if 'debian' in host.ansible.get_variables()["inventory_hostname"]:
        s = host.service("chrony")
    else:
        s = host.service("chronyd")
    assert s.is_running
    assert s.is_enabled


def test_NOT_ntpd_package(host):
    assert not host.package("ntp").is_installed
