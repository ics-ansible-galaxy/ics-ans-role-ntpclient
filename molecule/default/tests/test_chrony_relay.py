import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('relay')


def test_chrony_relay(host):
    if 'debian' in host.ansible.get_variables()["inventory_hostname"]:
        f = host.file("/etc/chrony/chrony.conf")
    else:
        f = host.file("/etc/chrony.conf")

    assert f.contains('allow 10.10.10.0/24')
    assert f.contains('allow 10.42.0.0/16')
